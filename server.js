const express = require('express');
const app = express();
const port = 8000;
const Caesar = require('caesar-salad').Caesar;
const caesarPassword = 'passwprd';

app.get('/', (req, res) => {
    console.log(req);
    res.send('Main Page');
});

app.get('/:path', (req, res) => {
    res.send(req.params.path);
});

app.get('/encode/:string', (req, res) => {
    res.send(Caesar.Cipher(caesarPassword).crypt(req.params.string));
});

app.get('/decode/:string', (req, res) => {
    res.send(Caesar.Decipher(caesarPassword).crypt(req.params.string));
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});